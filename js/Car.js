class Person {
    constructor(name, age) {
        this._name = name
        this.age = age
    }

    static people() {
        return new Person("Meghan", 10)
    }

    get name() {
        return this._name
    }

    set name(newName) {
        if(newName)
            this._name = newName
    }
}

// var person1 = new Person("Vinny", 5)
// console.log(person1.name)

// person1.age = 20
// console.log(person1.age)

person2 = Person.people()
console.log(person2.name)

// Working with arrays
arr = [
    new Person("Vince", 1),
    new Person("Jeff", 2),
    new Person("Meghan", 3)
]
arr.forEach(item => {
    console.log(item.name)
});