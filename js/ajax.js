var oAjax;

document.getElementById("btn").addEventListener("click", make_request);

// Execute HTTPRequest
function make_request() {
    oAjax = new XMLHttpRequest();
    oAjax.onreadystatechange = function() {
        show_results();
    }
    oAjax.open("GET", "./txt/hello.txt", true);
    oAjax.send(null);
}

function show_results() {
    if(oAjax.readyState===4 && oAjax.status===200) {
        document.getElementById("div1").innerHTML = oAjax.responseText;
    }
}