// document.getElementById("trafficLight").onclick = function() {changeColor()}

function changeColor() {
    var red = document.getElementById("red");
    var yellow = document.getElementById("yellow");
    var green = document.getElementById("green");
    var desc = document.getElementById("trafficLightDesc");

    if(green.style.backgroundColor === "green") {
        green.style.backgroundColor = "#555";
        yellow.style.backgroundColor = "yellow";
        desc.innerHTML = "SLOW DOWN";
    } else if(yellow.style.backgroundColor === "yellow") {
        yellow.style.backgroundColor = "#555";
        red.style.backgroundColor = "red";
        desc.innerHTML = "STOP";
    } else {
        red.style.backgroundColor = "#555";
        green.style.backgroundColor = "green";
        desc.innerHTML = "GO";
    }
}

// After 5-10 secs, the sign will pop up
var trafficJamAlert = () => {
    alert("TRAFFIC JAM!");
    document.getElementById('trafficLight').removeAttribute("onclick");
    document.getElementById("red").style.backgroundColor = "red";
    document.getElementById("yellow").style.backgroundColor = "#555";
    document.getElementById("green").style.backgroundColor = "#555";
    document.getElementById("trafficLightDesc").innerHTML = "TRAFFIC JAM";
};

var trafficJamClear = () => {
    alert("TRAFFIC JAM CLEARED");
    document.getElementById('trafficLight').addEventListener("click", changeColor);
    changeColor();
};

var rand = Math.floor(Math.random() * 5) + 5;
setTimeout(trafficJamAlert, rand * 1000);
rand += Math.floor(Math.random() * 5) + 5;
setTimeout(trafficJamClear, rand * 1000);
