// Delcare a variable
var myName = "Fanny"

// Changing the type of myName & changing the value
var myName = 5

// Declare a function
function myFunc(name) {
    var myFuncVar = "hi from myFunc"
    console.log(name)
}

// Calls the function
myFunc(myName)
myFunc()